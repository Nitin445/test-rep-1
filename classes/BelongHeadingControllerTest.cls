/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 18/05/2016
  @Description : Test Class for the BelongHeadingController.
*/
@isTest
public with sharing class BelongHeadingControllerTest
{
	@testSetup static void setupTestData() 
    {
	// Hello
        TestUtil.createKnowledgeSettings();
    }

	static testmethod void testGetterSetters()
    {
        Test.startTest();
        BelongHeadingController controller = new BelongHeadingController();

		BelongSupportHomeController BlngCntrl = controller.BlngCntrl;
	    BelongSupportSearchResultsController BlngSrchCon = controller.BlngSrchCon;
	    BelongSupportArticleController BlngArtCntrl = controller.BlngArtCntrl;
	    BelongSupportCategoryController BlngCatCntrl = controller.BlngCatCntrl;
	    Map<String,Public_Knowledge_URL_Mapping__c> knowledgeURL = controller.knowledgeURL;
	    String redirectURL = controller.redirectURL;        
	    String searchTerms = controller.searchTerms; 
	    Test.stopTest();
	}

	static testmethod void testBuildSearchURL()
    {
    	Test.startTest();
		
    	PageReference pageRef = Page.Belong_Support_Home;
        pageRef.getParameters().put('searchTerms','speed');
        Test.setCurrentPageReference(pageRef);

    	BelongHeadingController controller = new BelongHeadingController();
    	controller.buildSearchURL();
    	Test.stopTest();
    }
}