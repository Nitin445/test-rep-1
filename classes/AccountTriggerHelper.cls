/************************************************************************************************************
* Apex Class Name   : AccountTriggerHelper.cls
* Version           : 1.0 
* Created Date      : 24 JUN 2016
* Function          : Handler class for Account Object Trigger
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Mitali Telang               24/06/2016              Created Helper Class
************************************************************************************************************/

public with sharing class AccountTriggerHelper 
{
    
    /******************************************************************************
    Method Name: createEncodeKey
    Parameters: List of Accounts (Trigger.New)
    Return: Void
    Description: It will create an encoding key for the account to be used in surveys by the related contacts.
    Developer Name: Mitali Telang
    *****************************************************************************/
    public static void createEncodeKey(List<Account> newAccountList)
    {
        Blob key;
        for(Account acc: newAccountList)
        {
            if(acc.Encryption_Key__c == null || acc.Encryption_Key__c.length() <= 0)
            {
                key = Crypto.generateAesKey(128);
                String s = EncodingUtil.base64Encode(key);
                acc.Encryption_Key__c = s;
            }
        }
    }
}