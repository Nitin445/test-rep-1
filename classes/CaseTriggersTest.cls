/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Test class for the case trigger process
*/

@isTest
private class CaseTriggersTest
{
  public static Account sAccount;
  public static Contact sContact;
  public static Map<Id,Schema.RecordTypeInfo> caseRecordTypeInfoId;
  public static Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
  public static Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId;
  public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
  
    @testSetup static void setupTestData() 
    {
        caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
        
        sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Octane_Customer_Number__c  = '123456';
        sAccount.Customer_Status__c      	= 'Active';
        insert sAccount;
        
        sContact       			 = new Contact();
        sContact.FirstName  	 = 'Joetest';
        sContact.LastName 		 = 'Smithtest';
        sContact.AccountId  	 = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        sContact.Email  	 	 = 'Joe@test.com';
        insert sContact;
        
        Notification_Settings__c settings = new Notification_Settings__c();
		settings.SetupOwnerId=UserInfo.getOrganizationId();
		settings.First_Reminder_Before_Disconnection__c = 90;
		settings.Second_Reminder_Before_Disconnection__c = 60;
		settings.Third_Reminder_Before_Disconnection__c = 30;
		settings.Fourth_Reminder_Before_Disconnection_del__c = 14;
		insert settings;
        
        
    }
    
    static testmethod void testCaseTriggers()
    { 
    	caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
        Test.startTest();
        sAccount = [Select id FROM Account];
        sContact = [Select id FROM Contact];
        // Test inserting
        List<Case> caseList = new List<Case>();
        Case sCase = new Case();
        sCase.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase.AccountId = sAccount.Id;
        sCase.ContactId = sContact.Id;
        sCase.Complaint_Category__c = 'General';
        sCase.Status = 'Open';
        sCase.Origin = 'Phone';
        caseList.add(sCase);

        Case sCase2 = new Case();
        sCase2.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase2.AccountId = sAccount.Id;
        sCase2.ContactId = sContact.Id;
        sCase2.Complaint_Category__c   = 'TIO';
        sCase2.Status = 'Open';
        sCase2.Origin = 'Phone';
        caseList.add(sCase2);
        
        insert caseList;
		sCase = caseList[0];
		
        // Test updating
        sCase.Expected_Resolution_Execution_Date__c = null;
        sCase.Complaint_Category__c = 'High Risk';
        sCase.Resolution_Agreed__c = true;
        update sCase;

        // Test deleting
        delete sCase;
        Test.stopTest();
    }
    
    static testmethod void testRelatedCases()
    {
    	caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
        Test.startTest();
        sAccount = [Select id FROM Account];
        sContact = [Select id FROM Contact];
        
        Case sCase = new Case();
        sCase.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase.AccountId = sAccount.Id;
        sCase.ContactId = sContact.Id;
        sCase.Complaint_Category__c = 'General';
        sCase.Status = 'Open';
        sCase.Origin = 'Phone';
        sCase.Subject = 'Test Case';
        insert sCase;

        Case sCase2 = new Case();
        sCase2.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase2.AccountId = sAccount.Id;
        sCase2.ContactId = sContact.Id;
        sCase2.Complaint_Category__c = 'General';
        sCase2.Status = 'Open';
        sCase2.Origin = 'Phone';
        sCase2.Subject = 'Test Case';
        insert sCase2;
        
        sCase2 = [SELECT Id, ParentId FROM Case WHERE Id =: sCase2.Id];
        
        system.assertEquals(sCase.Id, sCase2.ParentId);
        Test.stopTest();
    }
    
    static testmethod void testChildCaseClose()
    {
    	caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
        Test.startTest();
        sAccount = [Select id FROM Account];
        sContact = [Select id FROM Contact];
        // Test inserting
        Case sCase = new Case();
        sCase.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase.AccountId = sAccount.Id;
        sCase.ContactId = sContact.Id;
        sCase.Complaint_Category__c = 'General';
        sCase.Status = 'Open';
        sCase.Origin = 'Phone';
        sCase.Subject = 'Test Case';
		insert sCase;

        Case sCase2 = new Case();
        sCase2.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
        sCase2.AccountId = sAccount.Id;
        sCase2.ContactId = sContact.Id;
        sCase2.ParentId = sCase.Id;
        sCase2.Complaint_Category__c = 'General';
        sCase2.Status = 'Open';
        sCase2.Origin = 'Phone';
        sCase2.Subject = 'Test Case';
        insert sCase2;
        
        sCase.Status = 'Closed';
        sCase.Product_and_Service__c = 'Speed boost';
        sCase.Reason = 'Existing problem';
        sCase.Root_Cause__c = 'NBN Appointment';
        sCase.Sub_Root_Cause__c = 'Other';
        
        update sCase;
        
        sCase2 = [SELECT Id, ParentId, Status, Reason FROM Case WHERE Id =: sCase2.Id];
        
        system.assertEquals(sCase2.Status, 'Closed');
        system.assertEquals(sCase2.Reason, 'Existing problem');
        
        Test.stopTest();
    }
    
    static testmethod void testCallBackLoad()
    {
    	caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    
	    id CaseCallbackRT = caseRecordTypeInfoName.get('Customer Call Back').getRecordTypeId();
	    
        Case sCase = new Case();
        scase.Belong_Id__c = '123456';
        sCase.Call_Back_Period__c = 'AM';
        sCase.Subject = 'Call Back Request From Web';
        sCase.RecordTypeId = CaseCallbackRT;
        
        insert sCase;
    }
    
    static testmethod void testADSLNBNCaseLoad()
    {
    	caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
	    
    	List<Case> caseList = New List<Case>();
        Case sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(100);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        scase.Belong_Id__c = '123456';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(80);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(50);
        caseList.add(sCase);
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(20);
        caseList.add(sCase);
        
        /*sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '123456';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(-1);
        caseList.add(sCase);*/
        
        sCase = new Case();
        sCase.Fix_National_Number__c = '889819617';
        sCase.Location_Id__c ='LOC000059940148';
        sCase.Is_NBN_Transition__c = true;
        sCase.NBN_Service_Class__c = '1';
        scase.Belong_Id__c = '951753';
        sCase.HFL_Disconnection_Date__c = System.today().addDays(50);
        caseList.add(sCase);
        
        try
        {
        	insert caseList;
        }
        catch (Exception e){}   
        
    }
    
    static testMethod void CaseTriggerDeleteTest() 
	{

		Test.startTest();
			sAccount = [Select id FROM Account];
	        sContact = [Select id FROM Contact];
	        caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	        Case caseObj = new Case();
	        caseObj.RecordTypeId = caseRecordTypeInfoName.get('Complaints').getRecordTypeId();
	        caseObj.AccountId = sAccount.Id;
	        caseObj.ContactId = sContact.Id;
	        caseObj.Complaint_Category__c = 'General';
	        caseObj.Status = 'Open';
	        caseObj.Origin = 'Phone';
	        caseObj.Subject = 'Test Case';
	        insert caseObj;

			delete caseObj;
			System.AssertEquals(0, [SELECT Id FROM Case LIMIT 10].size());
			
			undelete caseObj;
			System.AssertEquals(1, [SELECT Id FROM Case LIMIT 10].size());
			
		Test.stopTest();
	}
}