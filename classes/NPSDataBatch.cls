global class NPSDataBatch implements Database.Batchable<sObject>, Schedulable, Database.stateful, Database.AllowsCallouts {
String OAUT_TOKEN;
Date dtToBeExecuted = system.Today();
public String sQuery;

    public NPSDataBatch(String sQuery)
    {
        this.sQuery = sQuery;
    }
    
    public NPSDataBatch()
    {
        sQuery = Label.NPS_Query ;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        OAUT_TOKEN = ININServices.getOAuthToken();
        system.debug('sQuery:: ' + sQuery);
        return Database.getQueryLocator(sQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Task> lstTask)
    {
        
        ININServices.ININConversationWrapper oININConversationWrapper;
        List<Log__c> lstLog = new List<Log__c>();
        
        for(Task oTask: lstTask)
        {
            oININConversationWrapper = ININServices.getININConversationDetails(oTask.Interaction_Id__c, OAUT_TOKEN);
            system.debug('oININConversationWrapper:: ' + oININConversationWrapper);
            //if oAuth token gets expire then retry with new token
            if(oININConversationWrapper.Id == NULL)
            {
                OAUT_TOKEN = ININServices.getOAuthToken();
                oININConversationWrapper = ININServices.getININConversationDetails(oTask.Interaction_Id__c, OAUT_TOKEN);
            }
            
            if(oININConversationWrapper.oLog == NULL)
                oTask = updateTaskFromNPSWrapper(oTask, oININConversationWrapper.participants[0].attributes);
            else
                lstLog.add(oININConversationWrapper.oLog);
        }
        
        insert lstLog;        
        update lstTask;

    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext SC)
    {
        Database.executeBatch(this, 90);
    }
    
    private Task updateTaskFromNPSWrapper(Task oTask, ININServices.Attributes oNPSAttributes)
    {
        oTask.NPS_Score__c = oNPSAttributes.NPS_Score;
        oTask.NPS_Resolution__c = oNPSAttributes.NPS_Resolution;
        oTask.NPS_Verbatim__c = oNPSAttributes.NPS_Verbatim;
        oTask.NPS_Data_Recieved__c = true;
        return oTask;
    }
    
}