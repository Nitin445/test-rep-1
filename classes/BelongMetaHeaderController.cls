/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 18/0/2016
  @Description : Controller Class for the Belong MetaHeader component .
*/
public with sharing class BelongMetaHeaderController 
{
    public BelongSupportHomeController 			BlngCntrl 		{ get; set; }
    public BelongSupportSearchResultsController BlngSrchCon 	{ get; set; }
    public BelongSupportArticleController   	BlngArtCntrl 	{ get; set; }
    public BelongSupportCategoryController   	BlngCatCntrl 	{ get; set; }
    
    public BelongMetaHeaderController(){}
    
}