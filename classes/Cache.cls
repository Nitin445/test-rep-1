public with sharing class Cache 
{
	private static String defaultFields = 'Id, Name';
	private static Map<String, String> objQueries = new Map<String, String>
    {
		'Account' => 'Octane_Customer_Number__c, Encryption_Key__c',
		'Contact' => 'Phone, MobilePhone, Contact_Role__c, Id_Hashcode__c, AccountId, FirstName',
		'SMS_Template__c' => 'SMS_Template__c, Active__c, Available_to_FOH__c, Template_Category__c, Code__c',
		'Case' => 'Order_Health__c, Service__c, SLA_Missed__c, Unhealthy_Order_Status__c, Unhealthy_Status_Reason__c, '+
				'TELSTRA_REFERENCE__c, Customer_Octane_Number__c, Status, RecordTypeId, isClosed, contactId, CaseNumber,'+
                'Appointment_Date__c, Appointment_Start_Time__c, Appointment_End_Time__c, Request_Status__c, Segment_Status__c',
		'Order' => 'Activation_Date__c, isActive__c, Id_Hashcode__c, Survey_URL_Follow_Up__c, Survey_URL_Post_Activation__c, ShipToContactId'
    };

	/**
    * @description Object child queries relationship name. 
    */
    private static Map<String, List<ChildObject>> objChildQueries = new Map<String, List<ChildObject>>
    {
        'Account'         => new List<ChildObject>{ new ChildObject('Contact', 'Contacts'),
        											new ChildObject('Case', 'Cases'),
        											new ChildObject('Order', 'Orders')}
    };

	/**
    * @description  Object parent queries relationship name
    */
    private static Map<String, List<ParentObject>> objParentQueries = new Map<String, List<ParentObject>>
    {
        'Contact' => new List<ParentObject>{ new ParentObject('Account', 'Account')},
        'Order' => new List<ParentObject>{ new ParentObject('Account', 'Account')}
    };
    
    /**
		Gets the name of an sObject based on the prefix of an ID
	*/
    public static String getObjectNameFromId(Id objId)
    {
        String output;

        if (objId != null)
        {
            Schema.SObjectType sobjtype = objId.getSObjectType();

            if (sobjtype != null)
                output = sobjtype.getDescribe().getName();
        }

        return output;
    }
    
    //get first Item 
    private static Id getFirst(Set<Id> objs)
    {
        Id output;

        if (objs != null)
        {
            for (Id o: objs)
            {
                output = o;
                break;
            }
        }
        
        return output;
    }
    
    /*
		Retrieves a record and all of its fields based on the record ID
    */
    public static SObject getRecord(Id objId)
    {
        SObject output;

        if (objId == null)
            return output;

        String objType = getObjectNameFromId(objId);

        // Run query.
        String query = 'Select '+ defaultFields ;
        if (objQueries.containsKey(objType)) 
        	query += ',' + objQueries.get(objType);
        
        query += ' FROM ' + objType;
        query += ' WHERE Id=\''+objId+'\'';
		
		System.Debug(loggingLevel.Error, '@cache getRecord QUERY' + query);
        for (SObject o: Database.query(query))
            output = o;
		
		System.Debug(loggingLevel.Error, '@cache getRecord output' + output);
		
        return output;
    }
    
    public static Map<Id, SObject> getRecordMap(Set<Id> fieldIds) { return getRecordMap(fieldIds, 'Id'); }
    public static Map<Id, SObject> getRecordMap(Set<Id> fieldIds, String field)
    {
        Map<Id, SObject> output = new Map<Id, SObject>();

        for (SObject o: getRecords(fieldIds, field))
            output.put(o.Id, o);

        return output;
    }
    
    public static List<SObject> getRecords(Set<Id> fieldIds) { return getRecords(fieldIds, 'Id'); }
    public static List<SObject> getRecords(Set<Id> fieldIds, String field)
    {
        return Cache.getRecords(fieldIds, field, null);
    }
    
    public static List<SObject> getRecords(Set<Id> fieldIds, String field, String orderBy)
    {
        List<SObject> output = new List<SObject>();

        if (field == null || fieldIds == null || fieldIds.isEmpty())
            return output;
            
        String objType = getObjectNameFromId(getFirst(fieldIds));

        // Run query.
        String query = 'SELECT ' + defaultFields;
        if (objQueries.containsKey(objType)) 
        	query += ',' + objQueries.get(objType);
        	
        query += ' FROM ' + objType;
        query += ' WHERE '+ field + ' IN: fieldIds';

        if(orderBy != NULL)
            query += ' ORDER BY ' + orderBy;

        for (SObject o: Database.query(query))
            output.add(o);

        return output;
    }
    
    //Dynamic function to get records by an specified field and values for such field
	public static List<SObject> getRecordsFieldSetValue(String objType, String field, Set<String> fieldValues)
	{
		List<SObject> output = new List<SObject>();
		if (objType == NULL || field == NULL || fieldValues == NULL || fieldValues.isEmpty())
			return output;
		
		output = new List<SObject>();
		String query = 'SELECT ' + defaultFields;
		
		if(objQueries.containsKey(objType))
			query += ', ' + objQueries.get(objType);
		
		query += ' FROM ' + objType + ' ';
		query += ' WHERE ' + field + ' IN: fieldValues';
		
		for (SObject obj: Database.query(query))
		{
			output.add(obj);
		}
		return output;
	}
	
	public static SObject getRecordFieldSetValue(String objType, String field, Set<String> fieldValues)
	{
		List<SObject> outputList = new List<SObject>();
		SObject output;
		
		outputList = cache.getRecordsFieldSetValue(objType, field, fieldValues);
		if(!outputList.isEmpty())
		{
			output = outputList[0];
		}
		
		return output;
	}
    
    public static sObject getRecordsFromCustomerNumber(String custNo, List<String> childObjects)
    {
    	SObject output;

        if (custNo == null)
            return output;
        
        String objType = 'Account';
        
		// Run query.
        String query = 'Select '+ defaultFields ;
        if (objQueries.containsKey(objType)) 
        	query += ',' + objQueries.get(objType);
        
        if(!childObjects.isEmpty())
        {
        	for(String childObj : childObjects)
        	{
        		if (objQueries.containsKey(childObj))
        		{
        			system.debug(logginglevel.error, 'objChildQueries ' + objChildQueries);
        			system.debug(logginglevel.error, 'childObj ' + childObj);
        			String relationShip;
        			for(ChildObject chdObj: objChildQueries.get(objType))
        			{
        				if(chdObj.sObjName == childObj)
        					relationShip = chdObj.ReltionshipName;
        			}
        			query += ',( Select ';
        			query += objQueries.get(childObj);
        			query += ' From ' + relationShip;
        			query += ')';
        		}
        	}
        }
        
        query += ' FROM ' + objType;
        query += ' WHERE Octane_Customer_Number__c=\''+custNo+'\'';
		
		system.debug(logginglevel.error, 'QUERY ' + query);
		
		for (SObject o: Database.query(query))
            output = o;
		
		System.Debug(loggingLevel.Error, '@cache getRecordsFromCustomerNumber output' + output);
		
        return output;
    }
    
    public static List<sObject> getRecordsFromCustomerNumber(Set<String> custNos, List<String> childObjects)
    {
    	List<sObject> output = new List<sObject>();

        if (custNos == null)
            return output;
        
        System.debug(logginglevel.error, '@Cache getRecordsFromCustomerNumber');
        
        String objType = 'Account';
        
		// Run query.
        String query = 'Select '+ defaultFields ;
        if (objQueries.containsKey(objType)) 
        	query += ',' + objQueries.get(objType);
        
        if(!childObjects.isEmpty())
        {
        	for(String childObj : childObjects)
        	{
        		if (objQueries.containsKey(childObj))
        		{
        			String relationShip;
        			for(ChildObject chdObj: objChildQueries.get(objType))
        			{
        				if(chdObj.sObjName == childObj)
        					relationShip = chdObj.ReltionshipName;
        			}
        			query += ',( Select ';
        			query += objQueries.get(childObj);
        			query += ' From ' + relationShip;
        			query += ')';
        		}
        	}
        }
        
        query += ' FROM ' + objType;
        query += ' WHERE Octane_Customer_Number__c IN: custNos';
		 
		System.debug(logginglevel.error, '@Cache getRecordsFromCustomerNumber query' + query); 
		
		for (SObject o: Database.query(query))
            output.add(o);
		
		System.Debug(loggingLevel.Error, '@cache getRecordsFromCustomerNumber output' + output);
        return output;
    }
    
    public static Map<Id, SObject> getRecordMapFieldSetValue(String objType, String field, Set<String> fieldValues, List<String> childObjects)
    {
        Map<Id, SObject> output = new Map<Id, SObject>();

        for (SObject o: getRecordsFieldSetValue(objType, field, fieldValues, childObjects))
            output.put(o.Id, o);

        return output;
    }
	
	public static List<sObject> getRecordsFieldSetValue(String objType, String field, Set<String> fieldValues, List<String> childObjects) 
    {
    	List<sObject> output = new List<sObject>();

        if (fieldValues == null || String.Isblank(objType) || String.Isblank(field))
            return output;
        
        System.debug(logginglevel.error, '@Cache getRecordsFieldSetValue');
        
		// Run query.
        String query = 'Select '+ defaultFields ;
        if (objQueries.containsKey(objType)) 
        	query += ',' + objQueries.get(objType);
        
        if(!childObjects.isEmpty())
        {
        	for(String childObj : childObjects)
        	{
        		if (objQueries.containsKey(childObj))
        		{
        			String relationShip;
        			for(ChildObject chdObj: objChildQueries.get(objType))
        			{
        				if(chdObj.sObjName == childObj)
        					relationShip = chdObj.ReltionshipName;
        			}
        			query += ',( Select ';
        			query += objQueries.get(childObj);
        			query += ' From ' + relationShip;
        			query += ')';
        		}
        	}
        }
        
        query += ' FROM ' + objType;
        query += ' WHERE ' + field + ' IN: fieldValues';
		 
		System.debug(logginglevel.error, '@Cache getRecordsFieldSetValue query' + query); 
		
		for (SObject o: Database.query(query))
            output.add(o);
		
		System.Debug(loggingLevel.Error, '@cache getRecordsFieldSetValue output' + output);
        return output;
    }
    
    public class ChildObject
    {
        public String sObjName;
        public String ReltionshipName;

        public ChildObject(String sObj, String rlt)
        {
            sObjName = sObj;
            ReltionshipName = rlt;
        }
    }
    
    public class ParentObject
    {
        public String sObjName;
        public String ReltionshipName;

        public ParentObject(String sObj, String rlt)
        {
            sObjName = sObj;
            ReltionshipName = rlt;
        }
    }

    public static Map<String, SMS_Template__c> getAllSMSTemplates() 
    {
        Map<String, SMS_Template__c> output = new Map<String, SMS_Template__c>();
        
        System.debug(logginglevel.error, '@Cache getSMSTemplateFromSessionCache');
        
        List<SMS_Template__c> smsTemplates = [Select Id, Name, SMS_Template__c, Active__c, Available_to_FOH__c, Template_Category__c, Code__c From
                                             SMS_Template__c];
                
        for (SMS_Template__c templateObj: smsTemplates)
            output.put(templateObj.Code__c,templateObj);
        
        System.Debug(loggingLevel.Error, '@cache getSMSTemplateFromSessionCache output' + output);
        return output;

    }
}