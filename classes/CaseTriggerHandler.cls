/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 22/03/2016
  @Description : Account Trigger handler
*/

public with sharing class CaseTriggerHandler implements TriggerInterface
{
	private Boolean runAfterProcess = false;
	public static Boolean executeTrigger = true;
	
	/**
	*	This public method caches related data for before triggers, called once on start of the trigger execution
	*/
	public void cacheBefore()
	{
		System.debug(LoggingLevel.ERROR, '====>cacheBefore is called');
		CaseTriggerHelper.initializeProperties();
		if(!(trigger.isDelete || trigger.isUnDelete))
		{
			CaseTriggerHelper.collectContantCasesList();
			CaseTriggerHelper.collectBelongIdAccountReference();
		}
		
	}

	/**
	*	This public method caches related data for after triggers, called once on start of the trigger execution
	*/
	public void cacheAfter()
	{
		System.debug(LoggingLevel.ERROR, '====>cacheAfter is called');
		CaseTriggerHelper.initializeProperties();
	} 
	
	/**
	*	This public method process the business logic on 1 sObject at before insert
	*/
	public void beforeInsert(sObject newSObj)
	{
		/*System.debug(LoggingLevel.ERROR, 'Case handler beforeInsert is called');
		System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.beforeInsert newSObj:' + newSObj);*/
		if(CaseTriggerHandler.executeTrigger)
		{
			CaseTriggerHelper.setExpectedDates(newSObj, true);
			CaseTriggerHelper.setRecordType(newSObj);
			CaseTriggerHelper.setAccountRef(newSObj);
			CaseTriggerHelper.setContactData(newSObj);
			CaseTriggerHelper.setCallBackCaseIFBOTCase(newSObj); 
			CaseTriggerHelper.followUpMoveCase(newSObj);
        	
        	CaseTriggerHandler.executeTrigger = false;
			CaseTriggerHelper.relateParentCase(newSObj);
			CaseTriggerHandler.executeTrigger = true;
        }
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after insert
	*/
	public void afterInsert(sObject newSObj)
	{
		if(CaseTriggerHandler.executeTrigger)
		{
			CaseTriggerHelper.setAccountIFBOTfield(newSObj);
		}
	}
	
	/**
	*	This public method process the business logic on 1 sObject at before update
	*/
	public void beforeUpdate(sObject oldSObj, sObject newSObj)
	{
		/*System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.beforeUpdate');
		System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.beforeUpdate newSObj:' + newSObj);
		System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.beforeUpdate oldSObj:' + oldSObj);*/
		if(CaseTriggerHandler.executeTrigger)
		{
			CaseTriggerHelper.verifyExpectedDates(oldSObj, newSObj);
			CaseTriggerHelper.adjustNoCompliantADSLNBNCase(newSObj);
		}
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after update
	*/
	public void afterUpdate(sObject oldSObj, sObject newSObj)
	{
		/*System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.afterUpdate');
		System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.afterUpdate newSObj:' + newSObj);
		System.debug(LoggingLevel.ERROR,'====>CaseTriggerHandler.afterUpdate oldSObj:' + oldSObj);*/
		if(CaseTriggerHandler.executeTrigger)
		{
			CaseTriggerHelper.checkCaseClose(oldSObj, newSObj);
		}
	}
	
	/**
	*	This public method process the business logic on 1 sObject at before delete
	*/
	public void beforeDelete(sObject oldSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after delete
	*/
	public void afterDelete(sObject oldSObj)
	{
		
	}
	
	/**
	*	This public method process the business logic on 1 sObject at after undelete
	*/
	public void afterUndelete(sObject newSObj)
	{}
	
	/**
	*	Ater all rows have been processed this method will be called
	*	Usualy DML are placed in this method
	*/
	public void afterProcessing()
	{
		//Avoid calling DML if not required
		CaseTriggerHelper.closeChildCases();
		CaseTriggerHelper.updateAccountIFBOTcaseField();
	}	
	
}