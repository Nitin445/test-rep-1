/*
  @author  : Daniel Garzon(dgarzon@deloitte.com.au)
  @created : 22/03/2016
  @Description : Batch class, collect all cases type "ADSL NBN" which reminder date is today and enable flag "Send Reminder" so the process builder
  				"ADSL To NBL Transition Notification Process" will be executed.
*/
global class EnableADSLNBNRemindersBatch implements  Database.Batchable<sObject>, Database.stateful
{
	public String query = '';
	public String recordTypeId;
	public Notification_Settings__c notifSetting;
	
	global EnableADSLNBNRemindersBatch()
    {
        try
        {
        	notifSetting = Notification_Settings__c.getInstance();
        	Map<String,Schema.RecordTypeInfo> caseSTrgRecordTypeInfoName = GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
        	recordTypeId = caseSTrgRecordTypeInfoName.get('ADSL to NBN Transitions').getRecordTypeId();
        	
            query = 'SELECT id, Send_Reminder__c, Reminder_Stage__c, Reminder_Date__c, HFL_Disconnection_Date__c FROM case WHERE Reminder_Date__c = TODAY AND RecordTypeId =\''+ recordTypeId+'\'';
            
            if(Test.isRunningTest())
        		query += ' LIMIT 200';
            
        }catch(exception e){}
    }
    /**
     * return the query locator cursor handle
     */
    global  Database.Querylocator start (Database.Batchablecontext BC){
        try 
        {
            System.debug('Start: Batch Processings');
            return Database.getQueryLocator(query);
        } catch (Exception e) {system.debug('---error on start--'+e.getmessage()); return null;}
    }
    
   /**
     * perform the actual delete
   */
    global  void execute (Database.Batchablecontext BC, List<sObject> scope)
    {
    	List<Case> caseListUpdate = new List<Case>();
    	for(sObject obj : scope)
		{
			Case caseObj = (Case) obj;
			caseObj.Send_Reminder__c = true;
			
			//verify Reminder stage and update next reminder date and Reminder Stage
			integer notifDays;
			if(caseObj.Reminder_Stage__c == 'Initial Notification')
			{
				notifDays = integer.valueof(notifSetting.First_Reminder_Before_Disconnection__c) * -1;
				caseObj.Reminder_Date__c = caseObj.HFL_Disconnection_Date__c.addDays(notifDays);
				caseObj.Reminder_Stage__c = '1st Reminder (Email)';
			}
			else if(caseObj.Reminder_Stage__c == '1st Reminder (Email)')
			{
				notifDays = integer.valueof(notifSetting.Second_Reminder_Before_Disconnection__c) * -1;
				caseObj.Reminder_Date__c = caseObj.HFL_Disconnection_Date__c.addDays(notifDays);
				caseObj.Reminder_Stage__c = '2nd Reminder (Email)';
				
			}
			else if(caseObj.Reminder_Stage__c == '2nd Reminder (Email)')
			{
				notifDays = integer.valueof(notifSetting.Third_Reminder_Before_Disconnection__c) * -1;
				caseObj.Reminder_Date__c = caseObj.HFL_Disconnection_Date__c.addDays(notifDays);
				caseObj.Reminder_Stage__c = 'Final Notice (Email)';
			}
			else if(caseObj.Reminder_Stage__c == 'Final Notice (Email)')
			{
				notifDays = integer.valueof(notifSetting.Fourth_Reminder_Before_Disconnection_del__c) * -1;
				caseObj.Reminder_Date__c = caseObj.HFL_Disconnection_Date__c.addDays(notifDays);
				caseObj.Reminder_Stage__c = 'Final Notice (SMS)';
			}
			else if(caseObj.Reminder_Stage__c == 'Final Notice (SMS)')
			{
				caseObj.Reminder_Date__c = NULL;
				caseObj.Reminder_Stage__c = 'Disconnection Proceed';
			}
			
			caseListUpdate.add(caseObj);
		}
		
		//Update the cases
		if(!caseListUpdate.isEmpty())
			update caseListUpdate;
    }
    
     /**
     * post process
     */ 
    global void finish(Database.Batchablecontext BC)
    {
        System.debug('Finished Batch Processing ');
    } 
	
    
}