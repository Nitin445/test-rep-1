@isTest
private class EventTaskMigrationTest {

    public static testMethod void testEventMigration()
	{
		
		Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
		accRecordTypeInfoName		= GlobalUtil.getRecordTypeByName('Account');
		
    	Account sAccount 		= new Account();
        sAccount.RecordTypeId   = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name           = 'Test Account';
        sAccount.Phone          = '0412345678';
        sAccount.Customer_Status__c      = 'Active';
        sAccount.Octane_Customer_Number__c = '123456';
        insert sAccount;
        
        Contact sContact       	= new Contact();
        sContact.FirstName  	= 'Joe';
        sContact.LastName  		= 'Belong';
        sContact.AccountId  	= sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        List<Event> eventObjList = new List<Event>();
        Task taskObj = new Task();
        Event eventObj = new Event();
        eventObj.ActivityDateTime = system.Today();
		eventObj.Contact_Sub_Type__c = 'Test Subtype';
		eventObj.Contact_Type__c = 'Test Contact Type';
		eventObj.Description = 'Test Description';
		eventObj.Octane_ContactLog_ID__c = '95147879';
		eventObj.Octane_ContactLog_Username__c = 'scheduled-job-sqn';
		eventObj.Subject = 'SMS';
		eventObj.WhatId = sAccount.Id;
		eventObj.WhoId = sContact.Id;
		//eventObj.Status = 'Completed';
		eventObj.DurationInMinutes =1440;
		eventObjList.add(eventObj);
		
		eventObj = new Event();
		eventObj.ActivityDateTime = system.Today();
		eventObj.Contact_Sub_Type__c = 'Test Subtype';
		eventObj.Contact_Type__c = 'Test Contact Type';
		eventObj.Description = 'Test Description';
		eventObj.Octane_ContactLog_ID__c = '95147879';
		eventObj.Octane_ContactLog_Username__c = 'scheduled-job-sqn';
		eventObj.Subject = 'Email';
		eventObj.WhatId = sAccount.Id;
		eventObj.WhoId = sContact.Id;
		eventObj.DurationInMinutes =1440;
		eventObjList.add(eventObj);
		
		insert eventObjList;
		
		Test.startTest();
		
		EventTaskMigration eventMigration = new EventTaskMigration();
		database.executebatch(eventMigration);
		Test.stopTest();
	}
}