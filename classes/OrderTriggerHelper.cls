/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  @created : 07/07/2016
  @Description : Order Trigger Helper
*/
public with sharing class OrderTriggerHelper 
{
	private static Boolean isInitialized = false;
	
    private static Map<Id, Account> parentAccountKeyMap;
    private static Map<Id, String> orderIdHashId;
	
    public static void initializeProperties()
    {
        if(isInitialized)
            return;
        
        parentAccountKeyMap = new Map<Id, Account>(); 
        orderIdHashId = new Map<Id, String>();
        
        
        isInitialized               = true;
    }
    
    public static void collectParentAccount()
	{
		Set<String> accountIdSet = new Set<String>();
		//gathering all records id or belong id for query
		for(sObject orderObj : trigger.new)
		{
			if(orderObj.get('AccountId') != NULL)
				accountIdSet.add((String)orderObj.get('AccountId'));
		}
		
		//Put all related accounts in a map for future reference
		System.debug(LoggingLevel.ERROR,'====>collectParentAccount accountIdSet: ' + accountIdSet);
		Map<Id,Sobject> accObjMap = Cache.getRecordMapFieldSetValue('Account', 'Id', accountIdSet, new List<String>{'Contact'});
		for(SObject sObj : accObjMap.values())
		{
			Account accObj = (Account) sObj;
			parentAccountKeyMap.put(accObj.ID, accObj);
		}
		
	}
    
    public static void setPrimaryContactShipContact(sObject newSObj)
    {
    	if(String.IsBlank((Id)newSObj.get('ShipToContactId')))
    	{
    		Account accObj = parentAccountKeyMap.get((Id)newSObj.get('AccountId'));
    		System.debug(LoggingLevel.ERROR,'====>setPrimaryContactShipContact parentAccountKeyMap: ' + parentAccountKeyMap);
    		System.debug(LoggingLevel.ERROR,'====>setPrimaryContactShipContact accObj: ' + accObj);
    		for(Contact conObj: accObj.Contacts)
    		{
    			if(conObj.Contact_Role__c == 'Primary')
    			{
    				newSObj.put('ShipToContactId', conObj.Id);
    			}
    		}
    	}
    }
    
    //create a Hash code using the encription key from the account
    public static void createHashCode(sObject newSObj)
    {
    	if(string.isNotBlank((String)newSObj.get('AccountId')) && parentAccountKeyMap.containsKey((String)newSObj.get('AccountId')))
    	{
    		String accountKeyId = parentAccountKeyMap.get((String)newSObj.get('AccountId')).Encryption_Key__c;
    		Id OrderId	= (Id)newSObj.get('Id');
    		
	    	String hashCode = GlobalUtil.encrypt(OrderId, 128, EncodingUtil.base64Decode(accountKeyId));
	    	orderIdHashId.put(OrderId, hashCode);
    	}
    }
	
	//Update the hashcode value to the create contact record
    public static void updateOrderHashCode()
    {  
    	System.debug(LoggingLevel.ERROR,'====>updateOrderHashCode orderIdHashId: ' + orderIdHashId);
		if(orderIdHashId.size() > 0)
		{
			List<Order> orderList = new List<Order>();
	        for(SObject sobj: Cache.getRecords(orderIdHashId.keySet()))
	        {
	        	Order orderObj = (Order) sobj;
	        	orderObj.Id_Hashcode__c = orderIdHashId.get(orderObj.Id);
	        	orderList.add(orderObj);
	        }
	                
			OrderTriggerHandler.executeTrigger = false;
	        update orderList;
	        OrderTriggerHandler.executeTrigger = true;
		}
    }
    
    
    
    
    
}