global class SFWrapperObject
{
    global class OrderStatus
    {
        webservice String octaneNumber;
        webservice DateTime activationDate;
        webservice boolean isActive;
    }

    global class Notification
    {
    	webservice String octaneNumber;
    	webservice String braintreeResponse;
        webservice String smsTemplateCode;
        webservice String emailTemplateCode;
    	webservice List<String> eventAttributes;
    	webservice List<String> messageAttributes;
        webservice Account accountObj;
    }
}