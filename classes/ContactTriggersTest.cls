@isTest
private class ContactTriggersTest
{
    public static Map<Id,Schema.RecordTypeInfo> caseRecordTypeInfoId;
  	public static Map<String,Schema.RecordTypeInfo> caseRecordTypeInfoName;
  	public static Map<Id,Schema.RecordTypeInfo> accRecordTypeInfoId;
  	public static Map<String,Schema.RecordTypeInfo> accRecordTypeInfoName;
    public static Account sAccount;
    
    @testSetup static void setupTestData()
    {
        // Load RecordType
        caseRecordTypeInfoId	= GlobalUtil.getRecordTypeById('Case'); //getting all Recordtype for the Sobject
	    caseRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Case');//getting all Recordtype for the Sobject
	    accRecordTypeInfoId		= GlobalUtil.getRecordTypeById('Account'); //getting all Recordtype for the Sobject
	    accRecordTypeInfoName	= GlobalUtil.getRecordTypeByName('Account');//getting all Recordtype for the Sobject
        
        sAccount                            = new Account();
        sAccount.RecordTypeId               = accRecordTypeInfoName.get('Residential').getRecordTypeId();
        sAccount.Name                       = 'Test Account';
        sAccount.Phone                      = '0456123789';
        sAccount.Customer_Status__c      = 'Active';
        insert sAccount;
    }
    
  	static testmethod void ContactTrigger()
    {
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        System.assertNotEquals(sContact.Subscribe_Token__c,'');
        Test.stopTest();

    }
    
    static testmethod void createOnlyOnePrimaryContactPositiveTest(){
        
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        Contact sContact1 = new Contact();
        sContact1.FirstName  = 'SalesforceTest1';
        sContact1.LastName  = 'Smithtest1';
        sContact1.AccountId  = sAccount.Id;
        sContact1.Contact_Role__c = 'Delegated Authority';
        insert sContact1;
        
        System.assert(sContact.Id != NULL);
        
        Test.stopTest();        
        
    }
    
    static testmethod void createOnlyOnePrimaryContactNegativeTest(){
        
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact = new Contact();
            sContact.FirstName  = 'SalesforceTest1';
            sContact.LastName  = 'Smithtest1';
            sContact.AccountId  = sAccount.Id;
            sContact.Contact_Role__c = 'Primary';
            insert sContact;
        } catch(exception e){
            System.assert(e != NULL );
        }
        Test.stopTest();        
        
    }
    
    static testmethod void alwaysHavePrimaryContact(){
    
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact.Contact_Role__c = 'Delegated Authority';
            update sContact;
        } catch(exception e){
            System.assert(e != NULL );
        }
        Test.stopTest(); 
    }
    
    static testmethod void changeAccountNameTest(){
    
        Test.startTest();
        sAccount = [Select id FROM Account];
        Contact sContact = new Contact();
        sContact.FirstName  = 'SalesforceTest';
        sContact.LastName  = 'Smithtest';
        sContact.AccountId  = sAccount.Id;
        sContact.Contact_Role__c = 'Primary';
        insert sContact;
        
        try{
            sContact.FirstName  = 'Fname';
            sContact.LastName  = 'Lname';
            update sContact;
            
            Account acc = new Account();
            acc = [Select Name from Account where id = :sAccount.Id];
            System.assertEquals(acc.Name, sContact.FirstName+' '+ sContact.LastName);
        } catch(exception e){
            
        }
        Test.stopTest(); 
    }
}