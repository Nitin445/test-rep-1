global class UpdateHaskCode implements Database.Batchable<sObject>, Database.Stateful 
{
	// ************************************************************************************************
	// *** Static globals
	
	public String Query;
	
	// ************************************************************************************************
	// *** Globals
	
	// ************************************************************************************************
	// **** Executing Methods
	global UpdateHaskCode()
	{
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		Query = 'SELECT id, Encryption_Key__c, (SELECT Id, Id_Hashcode__c from Contacts ), (SELECT Id, Id_Hashcode__c from Orders) FROM Account WHERE Encryption_Key__c = NULL LIMIT 10000';
		//AND id=\'001p000000Ak1U8\'
		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope)
	{
		List<Account> updateAccountList = new List<Account>();
		List<Contact> updateContactList = new List<Contact>();
		List<Order> updateOrderList = new List<Order>();
		for (SObject obj: scope)
		{
			Account accObj = (Account)obj;
			
			Blob key = Crypto.generateAesKey(128);
            String s = EncodingUtil.base64Encode(key);
            accObj.Encryption_Key__c = s;
			updateAccountList.add(accObj);
			for(Contact conObj : accObj.Contacts)
			{
				String hashCode = GlobalUtil.encrypt(conObj.Id, 128, EncodingUtil.base64Decode(s));
				conObj.Id_Hashcode__c = hashCode;
				updateContactList.add(conObj);
			}
			
			for(Order orderObj : accObj.Orders)
			{
				String hashCode = GlobalUtil.encrypt(orderObj.Id, 128, EncodingUtil.base64Decode(s));
				orderObj.Id_Hashcode__c = hashCode;
				updateOrderList.add(orderObj);
			}
			
		}
		
		if(!updateAccountList.isEmpty())
			update updateAccountList;
		
		ContactTriggerHandler.executeTrigger = false;
		OrderTriggerHandler.executeTrigger = false;
		
		if(!updateContactList.isEmpty())
			update updateContactList;
		
		if(!updateOrderList.isEmpty())
			update updateOrderList;
			
		ContactTriggerHandler.executeTrigger = true;	
		OrderTriggerHandler.executeTrigger = true;
	}
	
	global void finish(Database.BatchableContext BC)
	{
		
	}
    
}