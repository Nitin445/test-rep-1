/************************************************************************************************************
* Apex Class Name   : SubscribeTokenTriggerHandler.cls
* Version           : 1.0 
* Created Date      : 25 JAN 2016
* Function          : Handler class for Contact Object SubscribeToken Triggers
* Modification Log  :
* Developer                   Date                    Description
* -----------------------------------------------------------------------------------------------------------
* Charlie Jiang               25/01/2016              Created Handler Class
************************************************************************************************************/

public with sharing class SubscribeTokenTriggerHandler
{
    // Used to prevent logic from re-executing within the same transaction
    public static boolean isExecuting = false;
    
    // BEFORE INSERT LOGIC
    public static void OnBeforeInsert(list<Contact> lstNewContacts)
    {
        if (isExecuting == false)
        {
            isExecuting = true;
            for(Contact contact : lstNewContacts){
              contact.Subscribe_Token__c = GuidUtil.NewGuid();
            }
            isExecuting = false;
        }
    }
}