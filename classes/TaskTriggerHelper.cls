public class TaskTriggerHelper{

static final String CONST_LEAD = 'Lead';
static final String CONST_CONTACT = 'Contact';
static final String CONST_SURVEYNAME = 'PostCallSurvey';
static final String CONST_QUESTION_SCORE = 'how likely are you to recommend Belong to your family and friends?';
static final String CONST_QUESTION_RESOLUTION = 'Did you get the help you needed?';
static final string CONST_COUNTRY_CODE = '+61';

static Map<String, String> mapQuestionToApiName = new Map<String, String>{ CONST_QUESTION_SCORE => 'NPS_Score__c',
                                                                   CONST_QUESTION_RESOLUTION => 'NPS_Resolution__c' };

static Id postCallSurveyId;
static Map<String, Id> mapSurveyQuestion;
static Map<Id, SurveyTaker__c> mapSurveyTaken;   
 
    public static void createPostCallSurveys(Map<Id, Task> mapNewTask, Map<Id, Task> mapOldTask)
    {

        mapSurveyTaken = new Map<Id, SurveyTaker__c>();
        
        Task oOldTask;
        for(Task oTask: mapNewTask.values())
        {
            oOldTask = mapOldTask.get(oTask.Id);
            if((String.isNotEmpty(oTask.NPS_Resolution__c) && oOldTask.NPS_Resolution__c != oTask.NPS_Resolution__c
            || String.isNotEmpty(oTask.NPS_Score__c) && oOldTask.NPS_Score__c != oTask.NPS_Score__c))
            {
                mapSurveyTaken.put(oTask.Id, createSurveyTaken(oTask.WhoId, oTask.OwnerId, oTask.Id));
            }
        }
        
        if(mapSurveyTaken.isEmpty())
            return;
        
        postCallSurveyId = getPostCallSurveyId();
        
        insert mapSurveyTaken.values();
        system.debug('mapSurveyTaken:: ' + mapSurveyTaken);    
        
        List<SurveyQuestionResponse__c> lstSurveyQuestionResponse = new List<SurveyQuestionResponse__c>();
        getSurveyQuestions();
        
        for(Id taskId: mapSurveyTaken.keyset())
        {
            createQuestionResponse(lstSurveyQuestionResponse, mapNewTask.get(taskId));
        }
        
        insert lstSurveyQuestionResponse;
            
    }
    
    private static void createQuestionResponse(List<SurveyQuestionResponse__c> lstSurveyQuestionResponse, Task oTask)
    {
        lstSurveyQuestionResponse.add(createQuestionResponse(oTask, CONST_QUESTION_RESOLUTION ));
        lstSurveyQuestionResponse.add(createQuestionResponse(oTask, CONST_QUESTION_SCORE ));
    }
    
    public static SurveyQuestionResponse__c createQuestionResponse(Task oTask, String sQuestion)
    {
        SurveyQuestionResponse__c oSurveyQuestionResponse = new SurveyQuestionResponse__c();
        oSurveyQuestionResponse.put('Response__c', (String)oTask.get(mapQuestionToApiName.get(sQuestion)));
        if(oSurveyQuestionResponse.Response__c == '0')
        {
           oSurveyQuestionResponse.Response__c = 'Yes'; 
        }else if(oSurveyQuestionResponse.Response__c == '1'){
            oSurveyQuestionResponse.Response__c = 'No'; 
        }
        oSurveyQuestionResponse.Survey_Date__c = system.Today();
        oSurveyQuestionResponse.Survey_Question__c = (Id)mapSurveyQuestion.get(sQuestion);
        oSurveyQuestionResponse.SurveyTaker__c = ((SurveyTaker__c)mapSurveyTaken.get(oTask.Id)).Id;
        system.debug('oSurveyQuestionResponse:: ' + oSurveyQuestionResponse);
        return oSurveyQuestionResponse;
    }
    
    private static void getSurveyQuestions()
    {
        mapSurveyQuestion = new Map<String, Id>();
        for(Survey_Question__c oSurveyQuestion: [SELECT Id, Name FROM Survey_Question__c WHERE Survey__r.Name =: CONST_SURVEYNAME])
        {
            mapSurveyQuestion.put(oSurveyQuestion.Name, oSurveyQuestion.Id);    
            system.debug('name:: ' + oSurveyQuestion.Name);
        }
        
        system.debug('mapSurveyQuestion:: ' + mapSurveyQuestion);                 
    }
    
    private static SurveyTaker__c createSurveyTaken(Id relatedToId, Id ownerId, Id taskId)
    {
        if(String.isEmpty(postCallSurveyId))
            postCallSurveyId = getPostCallSurveyId();
        
        SurveyTaker__c oSurveyTaker = new SurveyTaker__c();
        
        if(relatedToId != null)
        {
            if(CONST_CONTACT.equalsIgnoreCase(getObjectNameFromId(relatedToId)))
                oSurveyTaker.Contact__c = relatedToId;
            else if(CONST_LEAD.equalsIgnoreCase(getObjectNameFromId(relatedToId)))
                oSurveyTaker.Lead__c = relatedToId;
        }        

        oSurveyTaker.Survey__c = postCallSurveyId;
        oSurveyTaker.Taken__c = 'false';
        oSurveyTaker.OwnerId = ownerId;
        return oSurveyTaker;
    }
    
    private static Id getPostCallSurveyId()
    {
        Survey__c oPostCallSurvey = [SELECT Id FROM Survey__c WHERE Name =: CONST_SURVEYNAME ];
        return oPostCallSurvey.Id;
    }
    
    private static String getObjectNameFromId(Id recordId)
    {
        return recordId.getSObjectType().getDescribe().getName();
    }
    
    public static void updateTaskCallANIToAUSFormat(List<Task> lstTaskBeforeInsert)
    {
        String sCallANI = '';
        for(Task oTask: lstTaskBeforeInsert)
        {
            sCallANI = oTask.Call_ANI__c;
            if(String.isNotEmpty(sCallANI) && sCallANI.startsWith(CONST_COUNTRY_CODE))
            {
                sCallANI = sCallANI.replace(CONST_COUNTRY_CODE, '');
                sCallANI = !sCallANI.startsWith('0') ? 0 + sCallANI : sCallANI;    
                oTask.Call_ANI__c = sCallANI;
            }
        }
    }
    
    public static void updateWhatId(List<Task> lstTask)
    {
        set<Id> whoIdSet = new Set<Id>();
        for(Task oTask: lstTask)
        {
            if(oTask.WhoId != null && getObjectNameFromId(oTask.WhoId) == CONST_CONTACT)
            {
                whoIdSet.add(oTask.WhoId);
            }
        }
        
        Map<Id,Contact> contMap = new Map<Id,Contact>([SELECT Id,AccountId FROM Contact WHERE Id IN :whoIdSet]);
        for(Task oTask : lstTask){
            Contact oContact = contMap.get(oTask.WhoId);
            if(oContact != null && oContact.AccountId != null){
                oTask.WhatId = oContact.AccountId;
            }
        }
    }
}