/*
  @author  : Daniel Garzon(dgarzon@deloitte.com)
  			 Sathish Boopathy (SathishBabu.Boopathy@team.telstra.com)
  @created : 27/05/2016
  @Description : Test Class for the BelongSupportSearchResultsController.
*/
@isTest
public with sharing class BelongSupportSearchResultsControllerTest
{

	@testSetup static void setupTestData() 
    {
    	FAQ__kav faq = new FAQ__kav();
    	faq.title = 'test title';
    	faq.URLNAME = 'testUrl';
    	faq.Language  = 'en_US';
    	insert faq;

        TestUtil.createKnowledgeSettings();
    }

	static testmethod void testGetSearchArticle()
    {
    	Test.startTest();
    	BelongSupportSearchResultsController controller = new BelongSupportSearchResultsController();
    	controller.getSearchArticle();
    	Test.stopTest();
    }

    static testmethod void testBelongSupportSearchResultsController()
    {
    	Test.startTest();
    	FAQ__kav faq = new FAQ__kav();
    	ApexPages.StandardController sc = new ApexPages.StandardController(faq);
    	ApexPages.currentPage().getParameters().put('search','test title');
    	BelongSupportSearchResultsController controller = new BelongSupportSearchResultsController(sc);
    	Test.stopTest();
    }

    static testmethod void testGetterSetters()
    {
        Test.startTest();
        PageReference pageRef = Page.Belong_Support_Search_Results;
        pageRef.getParameters().put('currentSiteUrl','test');
        pageRef.getParameters().put('pageTitle','test');
        Test.setCurrentPageReference(pageRef);
        
        FAQ__kav faq = new FAQ__kav();
        ApexPages.StandardController sc = new ApexPages.StandardController(faq);
        ApexPages.currentPage().getParameters().put('search','test title');
        BelongSupportSearchResultsController controller = new BelongSupportSearchResultsController(sc);
        String currentSiteUrl = controller.currentSiteUrl;
        String pageTitle = controller.pageTitle;
        String FAQTitle = controller.FAQTitle;
        FAQ__kav faqTopicArticle = controller.faqTopicArticle;
        String publishStatus  = controller.publishStatus;
        String siteName = controller.siteName;
        List<FAQ__kav> FAQArticles = controller.FAQArticles;
        Test.stopTest();
    }
}