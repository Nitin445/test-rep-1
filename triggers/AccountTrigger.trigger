trigger AccountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
	//Trigger too small to implement handler and helper. review concept "Handler - Helper"i.e Case, if this becomes big

    if(trigger.isBefore && trigger.isInsert)
    {
        AccountTriggerHelper.createEncodeKey(trigger.New);
    }    
}