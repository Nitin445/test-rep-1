trigger processIndividualEmailResult on et4ae5__IndividualEmailResult__c (after insert,after update) {
    
     IndividualEmailResultTriggerHandler.UpdateEmailSent(Trigger.new, Trigger.isInsert);
}