<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Country_Field_update_in_Contact_Address</fullName>
        <description>This is to update the country field if blank or does not match Australia.</description>
        <field>MailingCountry</field>
        <formula>&quot;AUSTRALIA&quot;</formula>
        <name>Country Field update in Contact Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Country_Field_update_in_Shipping_Address</fullName>
        <description>This is to update the country field if blank or does not match Australia.</description>
        <field>OtherCountry</field>
        <formula>&quot;Australia&quot;</formula>
        <name>Country Field update in Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetSyncToSent</fullName>
        <description>Change the Sync Status field to &quot;Sent&quot;</description>
        <field>Sync_Status__c</field>
        <literalValue>Sent</literalValue>
        <name>SetSyncToSent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Authorisation_End_Date</fullName>
        <field>Authorisation_End_Date__c</field>
        <formula>today()</formula>
        <name>Update Authorisation End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Direct_Marketing_Opt_Out</fullName>
        <field>Direct_Marketing_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update Direct Marketing Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Door_to_Door_Opt_Out</fullName>
        <field>Door_to_Door_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update Door to Door Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Electronic_Direct_Marketing_Opt</fullName>
        <field>Electronic_Direct_Marketing_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update Electronic Direct Marketing Opt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Outbound_Telemarketing_Opt_Out</fullName>
        <field>Outbound_Telemarketing_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update Outbound Telemarketing Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SMS_MMS_Opt_Out</fullName>
        <field>SMS_MMS_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <name>Update SMS / MMS Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Contact_add_for_changing_of_leassee</fullName>
        <apiVersion>35.0</apiVersion>
        <description>Outbound message for manually adding a contact to an account with lease transfer from</description>
        <endpointUrl>https://www.belong.com.au/api/services/salesforce/contact/transfer/notification</endpointUrl>
        <fields>AccountId</fields>
        <fields>Birthdate</fields>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MobilePhone</fields>
        <fields>Phone</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Contact add for changing of leassee</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Contact_update_outbound</fullName>
        <apiVersion>35.0</apiVersion>
        <endpointUrl>https://www.belong.com.au/api/services/salesforce/contact/notification</endpointUrl>
        <fields>Birthdate</fields>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MailingCity</fields>
        <fields>MailingCountry</fields>
        <fields>MailingPostalCode</fields>
        <fields>MailingState</fields>
        <fields>MailingStreet</fields>
        <fields>MobilePhone</fields>
        <fields>Octane_Customer_Number__c</fields>
        <fields>OtherCity</fields>
        <fields>OtherCountry</fields>
        <fields>OtherPostalCode</fields>
        <fields>OtherState</fields>
        <fields>OtherStreet</fields>
        <fields>Phone</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Contact update outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Address Country Update for Contact Address</fullName>
        <actions>
            <name>Country_Field_update_in_Contact_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to populate the Country field in Addresses with &quot;Australia&quot; if the field is blank or not &quot;Australia&quot; for a given Contact address.</description>
        <formula>NOT(  ISBLANK( MailingStreet)&amp;&amp; ISBLANK( MailingCity) &amp;&amp;  ISBLANK(MailingState)&amp;&amp;  ISBLANK(MailingPostalCode) )  &amp;&amp; ( OR ( ISBLANK(MailingCountry), NOT (CONTAINS(&quot;AUSTRALIA:Australia&quot;, MailingCountry))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Address Country Update for Shipping address</fullName>
        <actions>
            <name>Country_Field_update_in_Shipping_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is to populate the Country field in Addresses with &quot;Australia&quot; if the field is blank or not &quot;Australia&quot; for a given Shipping address.</description>
        <formula>NOT(  ISBLANK(  OtherStreet )&amp;&amp; ISBLANK(  OtherCity ) &amp;&amp;  ISBLANK( OtherState )&amp;&amp;  ISBLANK( OtherPostalCode ) )  &amp;&amp; ( OR ( ISBLANK( OtherCountry ), NOT (CONTAINS(&quot;AUSTRALIA:Australia&quot;,  OtherCountry ))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Update %22Authorized%22 tickbox on Contact</fullName>
        <actions>
            <name>Update_Authorisation_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Context : &quot;Customers will want contacts to be removed from an account - they would like an end date added 

Add a tick box against a contact which is auto ticked &apos;authorised&apos; to remove a contact untick the box and Column in the view&quot;</description>
        <formula>ISCHANGED( Authorized__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact add for changing of lessee</fullName>
        <actions>
            <name>Contact_add_for_changing_of_leassee</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send outbound message to service layer to transfter service</description>
        <formula>Account.Lease_Transferred_from__c  &lt;&gt; &apos;&apos;   &amp;&amp;  ISPICKVAL(Contact_Role__c, &apos;Primary&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact update</fullName>
        <actions>
            <name>SetSyncToSent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_update_outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send outbound message to service layer to update Octane customer details status when updating customer contact in Salesforce, can also re-triggered by administrator by update sync status to send</description>
        <formula>((ISCHANGED( Email )  ||  ISCHANGED( MobilePhone )||  ISCHANGED( Phone ) || ISCHANGED(  FirstName  ) ||  ISCHANGED(  LastName  )  ||  ISCHANGED(  Birthdate  ) || ISCHANGED( OtherAddress )|| ISCHANGED(  MailingAddress )) &amp;&amp;  ISPICKVAL(Contact_Role__c, &apos;Primary&apos;))|| (ISCHANGED( Sync_Status__c ) &amp;&amp;  ISPICKVAL(Sync_Status__c, &apos;Send&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact update for changing of lessee</fullName>
        <actions>
            <name>Contact_add_for_changing_of_leassee</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Contact add for changing of lessee</description>
        <formula>NOT(ISNULL(Account.Lease_Transferred_from__c)) &amp;&amp; ISPICKVAL(Contact_Role__c, &apos;Primary&apos;) &amp;&amp;  ISCHANGED( Remediate__c )&amp;&amp; Remediate__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Opt Out %28All%29</fullName>
        <actions>
            <name>Update_Electronic_Direct_Marketing_Opt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Marketing_Opt_Out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This is to tick all marketing options available when &apos;Marketing Opt Out&apos; is ticked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
