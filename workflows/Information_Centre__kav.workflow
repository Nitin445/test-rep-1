<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_email_to_KM_user</fullName>
        <description>Approval email to KM user</description>
        <protected>false</protected>
        <recipients>
            <recipient>carly.donnellan@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>diane.kacinari@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michelle.rajalingam@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nimit.jain@team.telstra.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rachid.souccane@team.belong.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shitiz.brutta@team.belong.com.au</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>todd.anderson@team.belong.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KM_Templates/Information_Centre_email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Review_Date</fullName>
        <field>Review_Date__c</field>
        <formula>LastPublishedDate + 180</formula>
        <name>Update Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Lifecycle_Review_Status__c</field>
        <literalValue>Due for Review</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_lifecycle_review_status</fullName>
        <field>Lifecycle_Review_Status__c</field>
        <literalValue>Due for Review</literalValue>
        <name>Update lifecycle review status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Information Centre %3A Update Lifecycle Review Status</fullName>
        <actions>
            <name>Update_lifecycle_review_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To track the Lifecycle of an article</description>
        <formula>Review_Date__c &gt; today()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Information Centre %3A Update Review Date</fullName>
        <actions>
            <name>Update_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Information_Centre__kav.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
